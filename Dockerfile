FROM debian:10.5-slim

RUN apt update \
    && apt -y install --no-install-recommends gnupg2 awscli xz-utils \
    && apt -y clean \
    && useradd -ms /bin/bash fs_s3_backup

COPY fs_s3_backup /usr/bin
USER fs_s3_backup
WORKDIR /home/fs_s3_backup
ENTRYPOINT ["/usr/bin/fs_s3_backup"]
